﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.BusinessEntities.STORES
{
    public class Str_Entry_Goods
    {
        public int SGID { get; set; }
        public int PORDNO { get; set; }
        public string DCBILL_NO { get; set; }
        public DateTime DATE_TIME { get; set; }
        public string VEHICLE_NO { get; set; }
        public int QTY { get; set; }
        public string SEC_SERIAL_NO { get; set; }
        public string E_TIME { get; set; }
        public string REMARK { get; set; }


    }
}
