﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CalendarEvent
{
    public string operation { get; set; }
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }
    public string UserID { get; set; }

    
}
