﻿//=================================================================================
// PROJECT NAME  : UAIMS                                                           
// MODULE NAME   : CONFIGURATION SETING
// CREATION DATE : 03-NOVEMBER-2009                                               
// CREATED BY    : JITENDRA CHILATE
// MODIFIED BY   : 
// MODIFIED DESC : 
//=================================================================================

using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using IITMS;
using IITMS.UAIMS;
using IITMS.UAIMS.BusinessLayer.BusinessEntities;
using IITMS.UAIMS.BusinessLayer.BusinessLogic;
using IITMS.NITPRM;


public partial class ConfigurationSetting : System.Web.UI.Page
{
    UAIMS_Common objUCommon = new UAIMS_Common();
    Common objCommon = new Common();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["masterpage"] != null)
            objCommon.SetMasterPage(Page, Session["masterpage"].ToString());
        else
            objCommon.SetMasterPage(Page, "");
    }
    string back = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["obj"] != null)
        { back = Request.QueryString["obj"].ToString().Trim(); }


        if (!Page.IsPostBack)
        {
            ViewState["action"] = "update";

            //Check Session
            if (Session["userno"] == null || Session["username"] == null ||
                Session["usertype"] == null || Session["userfullname"] == null)
            {
                Response.Redirect("~/default.aspx");
            }
            else
            {
                if (Session["comp_code"] == null || Session["fin_yr"] == null)
                {
                    Session["comp_set"] = "NotSelected";
                    Response.Redirect("~/ACCOUNT/selectCompany.aspx");

                }
                else
                {
                    Session["comp_set"] = "";
                }


                //Page Authorization
                CheckPageAuthorization();

                divCompName.InnerHtml = Session["comp_name"].ToString().ToUpper();
                Page.Title = Session["coll_name"].ToString();


                PopulateDropDown();


            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string code_year = Session["comp_code"].ToString() + "_" + Session["fin_yr"].ToString();
            AccountConfigurationController objMGC = new AccountConfigurationController();
            AccountConfiguration objMainGroup = new AccountConfiguration();
            if (GridData.Rows.Count != 0)
            {
                int i = 0;
                for (i = 0; i < GridData.Rows.Count; i++)
                {
                    objMainGroup.ConfiguraionDesc = GridData.Rows[i].Cells[0].Text.ToString().Trim();

                    HiddenField hdn = GridData.Rows[i].FindControl("hdnid") as HiddenField;
                    if (hdn != null)
                    {

                        objMainGroup.ConfigId = Convert.ToInt16(hdn.Value);
                    }
                    else
                    { objMainGroup.ConfigId = 0; }

                    //if (objMainGroup.ConfigId == 3)
                    //{
                    //    if (Convert.ToInt32(objCommon.LookUp("ACC_" + Session["comp_code"].ToString() + "_TRANS", "count(*)", "")) > 0)
                    //    {
                    //        objCommon.DisplayUserMessage(UPDMainGroup, "Transaction is exists, You cannot change configuration for AUTOGENERATED VOUCHER NO. REQUIRED", this.Page);
                    //        continue;
                    //    }
                    //}

                    CheckBox chk = GridData.Rows[i].FindControl("chkCheck") as CheckBox;
                    if (chk != null)
                    {
                        if (chk.Checked == true)
                        { objMainGroup.ConfigValue = "Y"; }
                        else
                        { objMainGroup.ConfigValue = "N"; }
                    }
                    else
                    { objMainGroup.ConfigValue = "N"; }

                    CustomStatus cs = (CustomStatus)objMGC.AddUpdateConfiguration(objMainGroup, Session["comp_code"].ToString(), Session["fin_yr"].ToString());
                    if (cs.Equals(CustomStatus.RecordUpdated))
                    {
                        objCommon.DisplayMessage(UPDMainGroup, "Record Saved Successfully!!!", this);

                        //lblStatus.Text = "Record Saved Successfully!!!";
                    }
                    else
                    {  //need to add msg
                    }



                }

            }
            PopulateDropDown();
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "Configuration.btnSubmit_Click-> " + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();


    }



    #region User Defined Methods
    private void CheckPageAuthorization()
    {
        if (Request.QueryString["pageno"] != null)
        {
            //Check for Authorization of Page
            if (Common.CheckPage(int.Parse(Session["userno"].ToString()), Request.QueryString["pageno"].ToString(), int.Parse(Session["loginid"].ToString()), 0) == false)
            {
                Response.Redirect("~/notauthorized.aspx?page=ConfigurationSetting.aspx");
            }
            Common objCommon = new Common();
            objCommon.RecordActivity(int.Parse(Session["loginid"].ToString()), int.Parse(Request.QueryString["pageno"].ToString()), 0);
        }
        else
        {
            //Even if PageNo is Null then, don't show the page
            Response.Redirect("~/notauthorized.aspx?page=ConfigurationSetting.aspx");
        }
    }


    private void PopulateDropDown()
    {
        try
        {
            DataSet ds = objCommon.FillDropDown("ACC_" + Session["comp_code"].ToString() + "_CONFIG", "*", "", "CONFIGID not IN (1,2,4,5,6,7,8,10,11,13,22)", "CONFIGID");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //CASH AMOUNT LIMIT
                    DataView dv = ds.Tables[0].DefaultView;
                    dv.RowFilter = "CONFIGDESC<>'CASH AMOUNT LIMIT'";
                    DataTable dtfilter = dv.ToTable();
                    GridData.DataSource = dtfilter;
                    GridData.DataBind();
                    int i = 0;
                    for (i = 0; i < GridData.Rows.Count; i++)
                    {
                        CheckBox chk = GridData.Rows[i].FindControl("chkCheck") as CheckBox;
                        if (chk != null)
                        {
                            if (dtfilter.Rows[i]["PARAMETER"].ToString().Trim() == "Y")
                            {
                                chk.Checked = true;

                            }
                            else { chk.Checked = false; }

                        }
                        if (dtfilter.Rows[i]["CONFIGDESC"].ToString().Trim() == "VOUCHER WITH FOUR SIGN")
                        {
                            chk.Attributes.Add("onClick", "return ShowVoucherPrint();");
                        }
                    }



                }
                else
                {
                    GridData.DataSource = null;
                    GridData.DataBind();

                }

            }
            else
            {
                GridData.DataSource = null;
                GridData.DataBind();
            }
            for (int i = 0; i < GridData.Rows.Count; i++)
            {
                CheckBox chkCheck = GridData.Rows[i].FindControl("chkCheck") as CheckBox;
                HiddenField hdndesc = GridData.Rows[i].FindControl("hdndesc") as HiddenField;
                if (GridData.Rows[i].Cells[0].Text == "AUTOGENERATED VOUCHER NO. REQUIRED")
                {
                    if (Convert.ToInt32(objCommon.LookUp("ACC_" + Session["comp_code"].ToString() + "_TRANS", "count(*)", "transaction_type<>'OB'")) > 0)
                    {
                        chkCheck.Enabled = false;
                    }
                }
                if (GridData.Rows[i].Cells[0].Text == "VOUCHER NO SEPRATE FOR RCPT,PAY,CONT,JOUN")
                {
                    if (Convert.ToInt32(objCommon.LookUp("ACC_" + Session["comp_code"].ToString() + "_TRANS", "count(*)", "transaction_type<>'OB'")) > 0)
                    {
                        chkCheck.Enabled = false;
                    }
                }
                if (GridData.Rows[i].Cells[0].Text == "ALLOW VOUCHER NO RESETTING")
                {
                    if (Convert.ToInt32(objCommon.LookUp("ACC_" + Session["comp_code"].ToString() + "_TRANS", "count(*)", "transaction_type<>'OB'")) > 0)
                    {
                        chkCheck.Enabled = false;
                    }
                }
                if (GridData.Rows[i].Cells[0].Text == "ALLOW LEDGER CREATION OVER COMPANIES")
                {
                    chkCheck.Enabled = false;
                }
            }

        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ConfigurationSetting.PopulateDropdown-> " + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }

    private void Clear()
    {
        GridData.DataSource = null;
        GridData.DataBind();
        ViewState["action"] = "update";
        lblStatus.Text = "";

    }
    #endregion



    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(back + ".aspx?obj=config," + "back");
    }
    protected void GridData_SelectedIndexChanged(object sender, EventArgs e)
    {
        //for (int i = 0; i < GridData.Rows.Count; i++)
        //{
        //    HiddenField hdnDescId = GridData.FindControl("CONFIGID") as HiddenField;
        //    if (hdnDescId.Value.ToString() == "16")
        //    {
        //        string Script = string.Empty;
        //        string url = string.Empty;
        //        Script = "ShowVoucherPrint()";
        //        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "Script", Script, true);
        //    }
        //}
    }
}
