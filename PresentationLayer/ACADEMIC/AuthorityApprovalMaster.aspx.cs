﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using IITMS;
using IITMS.UAIMS;
using IITMS.UAIMS.BusinessLayer.BusinessEntities;
using IITMS.UAIMS.BusinessLayer.BusinessLogic;
using IITMS.SQLServer.SQLDAL;
using IITMS.UAIMS.BusinessLogicLayer.BusinessEntities.Academic;

using IITMS.UAIMS.BusinessLayer.BusinessLogic.BusinessLogicLayer.BusinessLogic.Academic;


public partial class ACADEMIC_AuthorityApprovalMaster : System.Web.UI.Page
{

    Common objCommon = new Common();
    UAIMS_Common objUCommon = new UAIMS_Common();
    AuthorityApproval OBJAAP = new AuthorityApproval();
    AuthorityApprovalcontroller objAAM = new AuthorityApprovalcontroller();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["masterpage"] != null)
        {
            objCommon.SetMasterPage(Page, Session["masterpage"].ToString());
        }
        else
        {
            objCommon.SetMasterPage(Page, "");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Session["userno"] == null || Session["username"] == null ||
                Session["usertype"] == null || Session["userfullname"] == null)
            {
                Response.Redirect("~/default.aspx");
            }
            else
            {
                //CheckPageAuthorization();
                Page.Title = Session["coll_name"].ToString();

                pnlAdd.Visible = false;
                pnlauthority.Visible = false;
                pnlEmpList.Visible = false;
                pnlbtn.Visible = false;
                pnlauthoritybtn.Visible = false;
                pnlList.Visible = true;
                FillAAuthority();
                BindListViewAAMaster();
                
                objCommon.FillDropDownList(ddldepartment, "ACD_DEPARTMENT", "DEPTNO", "DEPTNAME", "DEPTNO>0", "DEPTNO");
                objCommon.FillDropDownList(ddlApp, "ACD_AUTHORITY_APPROVAL_TYPE_MASTER", "AUTHORITY_APPROVAL_NO", "AUTHORITY_APPROVAL_TYPE", "AUTHORITY_APPROVAL_NO > 0 AND ISNULL(ACTIVESTATUS,0)=1", "AUTHORITY_APPROVAL_NO");
                ViewState["edit"] = "add";
                Session["action"] = "add";
                
               
            }
        }

    }


    private void CheckPageAuthorization()
    {
        if (Request.QueryString["pageno"] != null)
        {
            //Check for   Authorization of Page
            if (Common.CheckPage(int.Parse(Session["userno"].ToString()), Request.QueryString["pageno"].ToString(), int.Parse(Session["loginid"].ToString()), 0) == false)
            {
                Response.Redirect("~/notauthorized.aspx?page=AuthorityApprovalMaster.aspx");
            }
        }
        else
        {
            //Even if PageNo is Null then, don't show the page
            Response.Redirect("~/notauthorized.aspx?page=AuthorityApprovalMaster.aspx");
        }
    }


    protected void BindListViewAAMaster()
    
    {
        try
        {
            AuthorityApprovalcontroller OBJAA = new AuthorityApprovalcontroller();
            DataSet ds = OBJAA.GetAllAAMaster();
            
           if (ds.Tables[0].Rows.Count > 0)
            {
                btnShowReport.Visible = false;
                lvAAPath.DataSource = ds;
                lvAAPath.DataBind();



                Label lblappproval1 = (Label)lvAAPath.FindControl("lblappproval1");
                //lblappproval1.Text = ds.Tables[0].Rows[0]["APPROVAL1"].ToString() ;
                lblappproval1.Text = "APPROVAL1" + " (" + (ds.Tables[0].Rows[0]["APPROVAL1"].ToString()) + ")";

                Label lblappproval2 = (Label)lvAAPath.FindControl("lblappproval2");
                //lblappproval2.Text = ds.Tables[0].Rows[0]["APPROVAL2"].ToString();
                lblappproval2.Text = "APPROVAL2" + " (" + (ds.Tables[0].Rows[0]["APPROVAL2"].ToString()) + ")";

                Label lblappproval3 = (Label)lvAAPath.FindControl("lblappproval3");
                //lblappproval3.Text = ds.Tables[0].Rows[0]["APPROVAL3"].ToString();
                lblappproval3.Text = "APPROVAL3" + " (" + (ds.Tables[0].Rows[0]["APPROVAL3"].ToString()) + ")";


                Label lblappproval4 = (Label)lvAAPath.FindControl("lblappproval4");
                //lblappproval4.Text = ds.Tables[0].Rows[0]["APPROVAL4"].ToString();
                lblappproval4.Text = "APPROVAL4" + " (" + (ds.Tables[0].Rows[0]["APPROVAL4"].ToString()) + ")";


                Label lblappproval5 = (Label)lvAAPath.FindControl("lblappproval5");
                //lblappproval5.Text = ds.Tables[0].Rows[0]["APPROVAL5"].ToString();
                lblappproval5.Text = "APPROVAL5" + " (" + (ds.Tables[0].Rows[0]["APPROVAL5"].ToString()) + ")";
            }



            else
            {
                btnShowReport.Visible = false;
                lvAAPath.DataSource = null;
                lvAAPath.DataBind();
                lvAAPath.Visible = false;
            }
            
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.BindListViewAAMaster ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server.UnAvailable");
        }
    }
    private void FillAAuthority()
    {
        try
        {
            objCommon.FillDropDownList(ddlCollege, "USER_ACC CROSS APPLY STRING_SPLIT(UA_COLLEGE_NOS, ',') INNER JOIN ACD_COLLEGE_MASTER CM ON (CM.COLLEGE_ID=VALUE)", "DISTINCT value AS COLLEGE_ID", "COLLEGE_NAME", "value > 0", "COLLEGE_NAME");
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.FillAAuthority ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }

    }


    private void EnableDisable(int index)
    {
        if (ddlApp.SelectedValue == "1")
        {
            switch (index)
            {
                case 1:
                    if (ddlAA1.SelectedIndex == 0)
                    {
                        ddlAA2.SelectedIndex = 0;
                        ddlAA2.Enabled = false;
                        ddlAA3.SelectedIndex = 0;
                        ddlAA3.Enabled = false;
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA2, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                    }
                    else
                    {

                        ddlAA2.Enabled = true;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA2, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");

                        ddlAA3.SelectedIndex = 0;
                        ddlAA3.Enabled = false;
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString();
                    }

                    break;

                case 2:
                    if (ddlAA2.SelectedIndex == 0)
                    {
                        ddlAA3.SelectedIndex = 0;
                        ddlAA3.Enabled = false;
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA3, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString();
                    }
                    else
                    {
                        ddlAA3.Enabled = true;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA3, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString();
                    }
                    break;
                case 3:
                    if (ddlAA3.SelectedIndex == 0)
                    {
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN  (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA4, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString();
                    }
                    else
                    {
                        ddlAA4.Enabled = true;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN  (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA4, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString();
                    }

                    break;
                case 4:
                    if (ddlAA4.SelectedIndex == 0)
                    {
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + "," + ddlAA4.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA5, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString();
                    }
                    else
                    {
                        ddlAA5.Enabled = true;
                        string swhere = "ua_type not in(2)" + " and UA_NO NOT IN  (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + "," + ddlAA4.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA5, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");

                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString() + "->" + ddlAA4.SelectedItem.ToString();
                    }
                    break;
                case 5:
                    if (!(ddlAA4.SelectedIndex == 0))
                    {
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString() + "->" + ddlAA4.SelectedItem.ToString() + "->" + ddlAA5.SelectedItem.ToString();
                    }
                    break;
            }
        }
        else
        {
            switch (index)
            {
                case 1:
                    if (ddlAA1.SelectedIndex == 0)
                    {
                        ddlAA2.SelectedIndex = 0;
                        ddlAA2.Enabled = false;
                        ddlAA3.SelectedIndex = 0;
                        ddlAA3.Enabled = false;
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA2, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                    }
                    else
                    {

                        ddlAA2.Enabled = true;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA2, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");

                        ddlAA3.SelectedIndex = 0;
                        ddlAA3.Enabled = false;
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString();
                    }

                    break;

                case 2:
                    if (ddlAA2.SelectedIndex == 0)
                    {
                        ddlAA3.SelectedIndex = 0;
                        ddlAA3.Enabled = false;
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA3, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString();
                    }
                    else
                    {
                        ddlAA3.Enabled = true;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA3, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString();
                    }
                    break;
                case 3:
                    if (ddlAA3.SelectedIndex == 0)
                    {
                        ddlAA4.SelectedIndex = 0;
                        ddlAA4.Enabled = false;
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN  (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA4, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString();
                    }
                    else
                    {
                        ddlAA4.Enabled = true;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN  (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA4, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString();
                    }

                    break;
                case 4:
                    if (ddlAA4.SelectedIndex == 0)
                    {
                        ddlAA5.SelectedIndex = 0;
                        ddlAA5.Enabled = false;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + "," + ddlAA4.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA5, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString();
                    }
                    else
                    {
                        ddlAA5.Enabled = true;
                        string swhere = "ua_type not in(1,2)" + " and UA_NO NOT IN  (" + ddlAA1.SelectedValue + "," + ddlAA2.SelectedValue + "," + ddlAA3.SelectedValue + "," + ddlAA4.SelectedValue + ")";
                        objCommon.FillDropDownList(ddlAA5, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", swhere, "UA_NO");

                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString() + "->" + ddlAA4.SelectedItem.ToString();
                    }
                    break;
                case 5:
                    if (!(ddlAA4.SelectedIndex == 0))
                    {
                        txtAAPath.Text = ddlAA1.SelectedItem.ToString() + "->" + ddlAA2.SelectedItem.ToString() + "->" + ddlAA3.SelectedItem.ToString() + "->" + ddlAA4.SelectedItem.ToString() + "->" + ddlAA5.SelectedItem.ToString();
                    }
                    break;
            }
        }
    }
    private void Clear()
    {
        ddlApp.SelectedIndex = 0;
        ddlAA1.SelectedIndex = 0;
        ddlAA2.SelectedIndex = 0;
        ddlAA3.SelectedIndex = 0;
        ddlAA4.SelectedIndex = 0;
        ddlAA5.SelectedIndex = 0;
        ddlAA2.Enabled = false;
        ddlAA3.Enabled = false;
        ddlAA4.Enabled = false;
        ddlAA5.Enabled = false;
        txtAAPath.Text = string.Empty;
        ddlCollege.SelectedIndex = 0;
    }
    protected void ddlApp_SelectedIndexChanged(object sender, EventArgs e)
    {
      
            FillAAuthority();    
    }

    protected void ddlAA1_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            this.EnableDisable(1);
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ddlAA1_click ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }
    protected void ddlAA2_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            this.EnableDisable(2);

        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ddlAA12_SelectedIndexChanged ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }
    protected void ddlAA3_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            this.EnableDisable(3);
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ddlAA3_SelectedIndexChanged ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }
    protected void ddlAA4_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            this.EnableDisable(4);
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ddlAA4_SelectedIndexChanged ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }
    protected void ddlAA5_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            this.EnableDisable(5);
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ddlAA5_SelectedIndexChanged ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }   
    private void clearnew()
    {
        ddldepartment.SelectedIndex = 0;
        ddlApp.SelectedIndex = 0;
        ddlAA1.SelectedIndex = 0;
        ddlAA2.SelectedIndex = 0;
        ddlAA3.SelectedIndex = 0;
        ddlAA4.SelectedIndex = 0;
        ddlAA5.SelectedIndex = 0;
        ddlAA2.Enabled = false;
        ddlAA3.Enabled = false;
        ddlAA4.Enabled = false;
        ddlAA5.Enabled = false;
        txtAAPath.Text = string.Empty;
        ddlCollege.SelectedIndex = 0;
        lblApprover1.Text = "Approval 1";
        lblApprover2.Text = "Approval 2";
        lblApprover3.Text = "Approval 3";
        lblApprover4.Text = "Approval 4";
        lblApprover5.Text = "Approval 5";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clearnew();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        clearnew();
        pnlAdd.Visible = true;
        pnlList.Visible = false;
        pnlbtn.Visible = true;
        ddlAA2.Enabled = false;
        ddlAA3.Enabled = false;
        ddlAA4.Enabled = false;
        ddlAA5.Enabled = false;
        ViewState["action"] = "add";       
        pnlAAPaList.Visible = false;
        pnlauthority.Visible = false;
        DataSet dsStudent = objCommon.FillDropDown("ACD_Add_APPROVAL_MASTER", "APPROVAL1,APPROVAL2", "APPROVAL3,APPROVAL4,APPROVAL5", "", "");
        if (dsStudent.Tables[0].Rows.Count > 0)
        {
            lblApprover1.Text = lblApprover1.Text + " (" + dsStudent.Tables[0].Rows[0]["APPROVAL1"].ToString() + ")";
            lblApprover2.Text = lblApprover2.Text + " (" + dsStudent.Tables[0].Rows[0]["APPROVAL2"].ToString() + ")";
            lblApprover3.Text = lblApprover3.Text + " (" + dsStudent.Tables[0].Rows[0]["APPROVAL3"].ToString() + ")";
            lblApprover4.Text = lblApprover4.Text + " (" + dsStudent.Tables[0].Rows[0]["APPROVAL4"].ToString() + ")";
            lblApprover5.Text = lblApprover5.Text + " (" + dsStudent.Tables[0].Rows[0]["APPROVAL5"].ToString() + ")";
        }
        else
        {
            //objCommon.DisplayMessage(this.Page, "Enter Appprovals", this.Page);
        }
       
    }
    protected void btnEdit_Click1(object sender, ImageClickEventArgs e)
    {
        try
        {
            pnlbtn.Visible = true;
            ImageButton btnEdit = sender as ImageButton;
            int APPNO = int.Parse(btnEdit.CommandArgument);
            Session["APP_NO"] = int.Parse(btnEdit.CommandArgument);
            this.ShowDetails(APPNO);
            ViewState["edit"] = "edit";
            Session["action"] = "edit";
            pnlAdd.Visible = true;
            pnlList.Visible = false;
            pnlAAPaList.Visible = false;           
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.btnEdit_Click-> " + ex.Message + "" + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server Unavailable");
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        try
        {
            AuthorityApprovalcontroller OBJAA = new AuthorityApprovalcontroller();
            OBJAAP.AUTHORITY = Convert.ToInt32(ddlApp.SelectedValue);
            OBJAAP.AUTHORITY_NAME = ddlApp.SelectedItem.Text;
           
            if (ddlAA1.SelectedValue != string.Empty)
            {
                OBJAAP.APPROVAL_1 = Convert.ToInt32(ddlAA1.SelectedValue);
            }
            else
            {
                OBJAAP.APPROVAL_1 = 0;
            }
            string ua_type = objCommon.LookUp("USER_ACC", "UA_DEPTNO", "UA_NO=" + OBJAAP.APPROVAL_1);
            if (ddlAA2.SelectedValue != string.Empty)
            {
                OBJAAP.APPROVAL_2 = Convert.ToInt32(ddlAA2.SelectedValue);
            }
            else
            {
                OBJAAP.APPROVAL_2 = 0;
            }
            
            if (ddlAA3.SelectedValue != string.Empty)
            {
                OBJAAP.APPROVAL_3 = Convert.ToInt32(ddlAA3.SelectedValue);
            }
            else
            {
                OBJAAP.APPROVAL_3 = 0;
            }
            if (ddlAA4.SelectedValue != string.Empty)
            {
                OBJAAP.APPROVAL_4 = Convert.ToInt32(ddlAA4.SelectedValue);
            }
            else
            {
                OBJAAP.APPROVAL_4 = 0;
            }
            if (ddlAA5.SelectedValue != string.Empty)
            {
                OBJAAP.APPROVAL_5 = Convert.ToInt32(ddlAA5.SelectedValue);
            }
            else
            {
                OBJAAP.APPROVAL_5 = 0;
            }
            OBJAAP.AAPATH = Convert.ToString(txtAAPath.Text);
            OBJAAP.CREATED_BY = 1;
            OBJAAP.IP_ADDRESS = "::1";
            //int deptNo = Convert.ToInt32(objCommon.LookUp("USER_ACC", "UA_DEPTNO", "UA_COLLEGE_NOS IN(" + "'" + ddlCollege.SelectedValue + "')"));
            if (Session["action"] != null && Session["action"].ToString().Equals("edit"))
            {
                //Edit 
                OBJAAP.APP_NO = Convert.ToInt32(Session["APP_NO"]);
                CustomStatus cs = (CustomStatus)OBJAA.UpdateAAPath(OBJAAP, Convert.ToInt32(ddlCollege.SelectedValue), Convert.ToInt32(ddldepartment.SelectedValue));
                if (cs.Equals(CustomStatus.RecordUpdated))
                {
                    clearnew();
                    BindListViewAAMaster();
                    objCommon.DisplayMessage(this.Page, "Record Updated sucessfully", this.Page);
                    ViewState["edit"] = null;
                    Session["action"] = null;

                }
            }
            else
            {
                //Add New
                int idno = 0;
                idno = OBJAA.AddAAPath(OBJAAP, Convert.ToInt32(ddlCollege.SelectedValue), Convert.ToInt32(ddldepartment.SelectedValue));
                if (idno == 2627)
                {
                    objCommon.DisplayMessage(this.Page, "Record already exist", this.Page);
                }
                
                else if (idno!=2727)
                {
                    clearnew();
                    BindListViewAAMaster();
                    objCommon.DisplayMessage(this.Page, "Record added successfully", this.Page);                   
                }
               
                else
                {
                    objCommon.DisplayMessage(this.Page, "Error", this.Page);

                }
            }

        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.btnSave_Click ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }
    private void ShowDetails(Int32 APP_NO)
    {
        DataSet ds = null;
        try
        {
            ds = objAAM.GetSingleAAPath(APP_NO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["APP_NO"] = APP_NO;
                ddlApp.SelectedValue = ds.Tables[0].Rows[0]["AUTHORITY_TYPE_NO"].ToString();
                FillAAuthority();
                ddlCollege.SelectedValue = ds.Tables[0].Rows[0]["COLLEGE_NO"].ToString().Trim().Equals(string.Empty) || ds.Tables[0].Rows[0]["COLLEGE_NO"].ToString().Trim().Equals(string.Empty) ? "0" : ds.Tables[0].Rows[0]["COLLEGE_NO"].ToString().Trim();

                objCommon.FillDropDownList(ddlAA1, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", "UA_TYPE NOT in(2) AND " + ddlCollege.SelectedValue + " IN (select value from dbo.Split(UA_COLLEGE_NOS,','))", "UA_NO");
                txtAAPath.Text = ds.Tables[0].Rows[0]["APP_PATH"].ToString();
                ddlAA1.SelectedValue = ds.Tables[0].Rows[0]["APPROVAL_1_UANO"].ToString();
                ddldepartment.SelectedValue = ds.Tables[0].Rows[0]["dept_noaa"].ToString();
                this.EnableDisable(1);

                if (!(ds.Tables[0].Rows[0]["APPROVAL_2_UANO"].ToString().Trim().Equals("0")))
                {
                    ddlAA2.SelectedValue = ds.Tables[0].Rows[0]["APPROVAL_2_UANO"].ToString();
                    this.EnableDisable(2);
                    ddlAA2.Enabled = true;
                }
                if (!(ds.Tables[0].Rows[0]["APPROVAL_3_UANO"].ToString().Trim().Equals("0")))
                {
                    ddlAA3.SelectedValue = ds.Tables[0].Rows[0]["APPROVAL_3_UANO"].ToString();
                    this.EnableDisable(3);
                    ddlAA3.Enabled = true;
                }
                if (!(ds.Tables[0].Rows[0]["APPROVAL_4_UANO"].ToString().Trim().Equals("0")))
                {
                    ddlAA4.SelectedValue = ds.Tables[0].Rows[0]["APPROVAL_4_UANO"].ToString();
                    this.EnableDisable(4);
                    ddlAA4.Enabled = true;
                }
                if (!(ds.Tables[0].Rows[0]["APPROVAL_5_UANO"].ToString().Trim().Equals("0")))
                {
                    ddlAA5.SelectedValue = ds.Tables[0].Rows[0]["APPROVAL_5_UANO"].ToString();
                    this.EnableDisable(5);
                    ddlAA5.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ShowDetails ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        clearnew();
        pnlAdd.Visible = false;
        pnlList.Visible = true;
        pnlbtn.Visible = false;
        pnlAAPaList.Visible = true;
        pnlAuthapprovalList.Visible = false;
        pnlauthority.Visible = false;
        //ViewState["action"] = "null";
    }
    protected void ddlCollege_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlApp.SelectedValue == "1")
            {
                if (ddlCollege.SelectedIndex > 0)
                {
                    objCommon.FillDropDownList(ddlAA1, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", "UA_TYPE NOT in(2) AND " + ddlCollege.SelectedValue + " IN (select value from dbo.Split(UA_COLLEGE_NOS,','))", "UA_NO");
                }
            }
            else
            {
                if (ddlCollege.SelectedIndex > 0)
                {
                    objCommon.FillDropDownList(ddlAA1, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", "UA_TYPE NOT IN (1,2) AND " + ddlCollege.SelectedValue + " IN (select value from dbo.Split(UA_COLLEGE_NOS,','))", "UA_NO");
                }

            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    protected void btnShowapprovedstud_Click(object sender, EventArgs e)
    {
        //int userNo = Convert.ToInt32(Session["userno"].ToString());
        //int userType = Convert.ToInt32(Session["usertype"].ToString());

        //try
        //{
        //    DataSet dsColleges = objSC.GetCollegesByUser_For_NoDues(Convert.ToInt32(Session["userno"]));
        //    string college = string.Empty;
        //    string actual_College = string.Empty;
        //    if (dsColleges.Tables.Count > 0)
        //    {
        //        lvclubRegapprove.DataSource = ds;
        //        lvclubRegapprove.DataBind();
        //        foreach (ListViewDataItem item in lvclubRegapprove.Items)
        //        {

        //            CheckBox status = item.FindControl("chkapprove") as CheckBox;
        //            Label lblstatus = item.FindControl("lbltype") as Label;

        //            string approve = lblstatus.ToolTip;

        //            if (approve == "1")
        //            {
        //                status.Checked = true;
        //                status.Enabled = false;

        //            }
        //            if (approve == "0")
        //            {
        //                status.Checked = false;

        //            }

        //        }

        //    }
        //    else
        //    {
        //        lvclubRegapprove.DataSource = null;
        //        lvclubRegapprove.DataBind();
        //        objCommon.DisplayMessage(this.Page, "No Record Found for current selection", this);

        //    }


        //}
        //catch (Exception ex)
        //{
        //    if (Convert.ToBoolean(Session["error"]) == true)
        //        objUCommon.ShowError(Page, "TRAININGANDPLACEMENT_Masters_TPJobLoc.BindListViewJobLoc -> " + ex.Message + " " + ex.StackTrace);
        //    else
        //        objUCommon.ShowError(Page, "Server UnAvailable");
        //}
    }


    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddldepartment.SelectedIndex > 0)
        {
            //objCommon.FillDropDownList(ddlAA1, "user_acc", "UA_NO", "UA_FULLNAME", "UA_TYPE =8 AND UA_COLLEGE_NOS IN(" + "'" + ddlCollege.SelectedValue + "'" + ") AND UA_DEPTNO LIKE '%" + ddldepartment.SelectedValue + "%'", "UA_NO");

            objCommon.FillDropDownList(ddlAA1, "user_acc", "UA_NO", "UA_NAME +' - '+ UA_FULLNAME COLLATE DATABASE_DEFAULT AS UANAME", "UA_TYPE !=2 AND " + ddlCollege.SelectedValue + " IN (select value from dbo.Split(UA_COLLEGE_NOS,',')) AND " + ddldepartment.SelectedValue + " IN (select value from dbo.Split(UA_DEPTNO,','))", "UA_NO");
        }
    }


    //added by pooja for nodues on date 21-07-2023
    protected void btnaddauthority_Click(object sender, EventArgs e)
    {
        clearnew();
        pnlAdd.Visible = false;
        pnlauthority.Visible = true;
        pnlList.Visible = false;
        pnlbtn.Visible = false;
        pnlauthoritybtn.Visible = true;
        ViewState["editauth"] = "add";
        Session["actionauth"] = "add";
        ViewState["actionauth"] = "add";
        pnlAAPaList.Visible = false;
        pnlAuthapprovalList.Visible = true;
        BindListViewAuthApprovalMaster();
    }

    protected void BindListViewAuthApprovalMaster()
    {
        try
        {
            AuthorityApprovalcontroller OBJAA = new AuthorityApprovalcontroller();
            DataSet ds = OBJAA.GetAllAuthApprovalPathMaster();

            if (ds.Tables[0].Rows.Count > 0)
            {
                btnShowReport.Visible = false;
                lvAuthapprovalList.DataSource = ds;
                lvAuthapprovalList.DataBind();
                lvAuthapprovalList.Visible = true;
            }
            else
            {
                btnShowReport.Visible = false;
                lvAuthapprovalList.DataSource = null;
                lvAuthapprovalList.DataBind();
                lvAuthapprovalList.Visible = false;
            }

        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.BindListViewAAMaster ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server.UnAvailable");
        }
    }

    protected void btnauthSave_Click(object sender, EventArgs e)
    {
        try
        {
            AuthorityApprovalcontroller OBJAA = new AuthorityApprovalcontroller();
            int AUTHORITYTYPE = Convert.ToInt32(ddlauthorityapproval.SelectedValue);
            string AUTHORITYNAME = ddlauthorityapproval.SelectedItem.Text;
            string APPROVAL1 = txtauthapproval1.Text;
            string APPROVAL2 = txtauthapproval2.Text;
            string APPROVAL3 = txtauthapproval3.Text;
            string APPROVAL4 = txtauthapproval4.Text;
            string APPROVAL5 = txtauthapproval5.Text;
         
            OBJAAP.CREATED_BY = 1;
            OBJAAP.IP_ADDRESS = "::1";

            if (Session["actionauth"] != null && Session["actionauth"].ToString().Equals("edit"))
            {
                //Edit 
                OBJAAP.APP_NO = Convert.ToInt32(Session["APP_NO"]);
                CustomStatus cs = (CustomStatus)OBJAA.UpdateAuthApprovalPath(OBJAAP, Convert.ToInt32(ddlauthorityapproval.SelectedValue), AUTHORITYNAME, APPROVAL1, APPROVAL2, APPROVAL3, APPROVAL4, APPROVAL5);
                if (cs.Equals(CustomStatus.RecordUpdated))
                {
                    clearnewAuthApproval();
                    BindListViewAuthApprovalMaster();
                    objCommon.DisplayMessage(this.Page, "Record Updated sucessfully", this.Page);
                    ViewState["editauth"] = null;
                    Session["actionauth"] = null;

                }
            }
            else
            {
                //Add New
                int idno = 0;
                idno = OBJAA.AddAuthApprovalPath(OBJAAP, Convert.ToInt32(ddlauthorityapproval.SelectedValue), AUTHORITYNAME, APPROVAL1, APPROVAL2, APPROVAL3, APPROVAL4, APPROVAL5);
                if (idno == 2627)
                {
                    objCommon.DisplayMessage(this.Page, "Record already exist", this.Page);
                }
                if (idno == 1027)
                {
                   // objCommon.DisplayMessage(this.Page, "", this.Page);
                }

                else if (idno != 2727)
                {
                   
                    clearnewAuthApproval();
                    BindListViewAuthApprovalMaster();
                    objCommon.DisplayMessage(this.Page, "Record added successfully", this.Page);
                    //ViewState["actionauth"] = "add";
                }

                else
                {
                    objCommon.DisplayMessage(this.Page, "Error", this.Page);

                }
            }

        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.btnSave_Click ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }

    private void clearnewAuthApproval()
    {
        ddlauthorityapproval.SelectedIndex = 0;
        txtauthapproval1.Text = string.Empty;
        txtauthapproval2.Text = string.Empty;
        txtauthapproval3.Text = string.Empty;
        txtauthapproval4.Text = string.Empty;
        txtauthapproval5.Text = string.Empty;
        //["action"] = "add";
        ViewState["actionauth"] = "add";
        ViewState["APP_NO"] = null;
    }
    protected void btnauthCancel_Click(object sender, EventArgs e)
    {
        clearnewAuthApproval();
        
        //Response.Redirect(Request.Url.ToString());
    }
    protected void btnauthBack_Click(object sender, EventArgs e)
    {
        clearnewAuthApproval();
        pnlAdd.Visible = false;
        pnlList.Visible = true;
        pnlbtn.Visible = false;
        pnlauthoritybtn.Visible = false;
        pnlAAPaList.Visible = true;
        pnlAuthapprovalList.Visible = false;
        pnlauthority.Visible = false;

       
    }
    protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            pnlauthoritybtn.Visible = true;
            ImageButton btnauthappEdit = sender as ImageButton;
            int APPNO = int.Parse(btnauthappEdit.CommandArgument);
            Session["APP_NO"] = int.Parse(btnauthappEdit.CommandArgument);
            this.ShowDetailsAuthApprovalPath(APPNO);
            ViewState["editauth"] = "edit";
            Session["actionauth"] = "edit";
            pnlauthority.Visible = true;
            pnlList.Visible = false;
            pnlAuthapprovalList.Visible = true;           
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.btnEdit_Click-> " + ex.Message + "" + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server Unavailable");
        }
    }
    private void ShowDetailsAuthApprovalPath(Int32 APP_NO)
    {
        DataSet ds = null;
        try
        {
            ds = objAAM.GetSingleAuthApprovalPath(APP_NO);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["APP_NO"] = APP_NO;                
                ddlauthorityapproval.SelectedValue = ds.Tables[0].Rows[0]["AUTHORITY_TYPE_NO"].ToString();
                txtauthapproval1.Text = ds.Tables[0].Rows[0]["APPROVAL1"].ToString();
                txtauthapproval2.Text = ds.Tables[0].Rows[0]["APPROVAL2"].ToString();
                txtauthapproval3.Text = ds.Tables[0].Rows[0]["APPROVAL3"].ToString();
                txtauthapproval4.Text = ds.Tables[0].Rows[0]["APPROVAL4"].ToString();
                txtauthapproval5.Text = ds.Tables[0].Rows[0]["APPROVAL5"].ToString();
                
            }
        }
        catch (Exception ex)
        {
            if (Convert.ToBoolean(Session["error"]) == true)
                objUCommon.ShowError(Page, "ACADEMIC_AuthorityApprovalMaster.ShowDetails ->" + ex.Message + " " + ex.StackTrace);
            else
                objUCommon.ShowError(Page, "Server UnAvailable");
        }
    }
}