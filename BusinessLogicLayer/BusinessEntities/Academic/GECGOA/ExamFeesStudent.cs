﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace IITMS.UAIMS.BusinessLayer.BusinessEntities
{
    public class ExamFeesStudent
    {
       
        
        private int _SCHEMENO;

        public int SCHEMENO
        {
            get { return _SCHEMENO; }
            set { _SCHEMENO = value; }
        }

        
        private string _EXAM_FEE_TRX_NO;

        public string EXAM_FEE_TRX_NO
        {
            get { return _EXAM_FEE_TRX_NO; }
            set { _EXAM_FEE_TRX_NO = value; }
        }

        private int _IDNO;

        public int IDNO
        {
            get { return _IDNO; }
            set { _IDNO = value; }
        }

        private string _REGNO;

        public string REGNO
        {
            get { return _REGNO; }
            set { _REGNO = value; }
        }

        private System.Nullable<int> _SESSIONNO;

        public System.Nullable<int> SESSIONNO
        {
            get { return _SESSIONNO; }
            set { _SESSIONNO = value; }
        }

        private System.Nullable<int> _SEMESTERNO;

        public System.Nullable<int> SEMESTERNO
        {
            get { return _SEMESTERNO; }
            set { _SEMESTERNO = value; }
        }

        private System.Nullable<int> _CATEGORYNO;

        public System.Nullable<int> CATEGORYNO
        {
            get { return _CATEGORYNO; }
            set { _CATEGORYNO = value; }
        }

        private System.Nullable<int> _EXAM_PTYPE;

        public System.Nullable<int> EXAM_PTYPE
        {
            get { return _EXAM_PTYPE; }
            set { _EXAM_PTYPE = value; }
        }

        private System.Nullable<int> _EXAMTYPE;

        public System.Nullable<int> EXAMTYPE
        {
            get { return _EXAMTYPE; }
            set { _EXAMTYPE = value; }
        }

        private System.Nullable<int> _COURSENO;

        public System.Nullable<int> COURSENO
        {
            get { return _COURSENO; }
            set { _COURSENO = value; }
        }

        private string _CCODE;

        public string CCODE
        {
            get { return _CCODE; }
            set { _CCODE = value; }
        }

        private System.Nullable<int> _SUBID;

        public System.Nullable<int> SUBID
        {
            get { return _SUBID; }
            set { _SUBID = value; }
        }

        private System.Nullable<int> _S1FEES;

        public System.Nullable<int> S1FEES
        {
            get { return _S1FEES; }
            set { _S1FEES = value; }
        }

        private System.Nullable<int> _S2FEES;

        public System.Nullable<int> S2FEES
        {
            get { return _S2FEES; }
            set { _S2FEES = value; }
        }

        private System.Nullable<int> _S3FEES;

        public System.Nullable<int> S3FEES
        {
            get { return _S3FEES; }
            set { _S3FEES = value; }
        }

        private System.Nullable<int> _S4FEES;

        public System.Nullable<int> S4FEES
        {
            get { return _S4FEES; }
            set { _S4FEES = value; }
        }

        private System.Nullable<int> _S5FEES;

        public System.Nullable<int> S5FEES
        {
            get { return _S5FEES; }
            set { _S5FEES = value; }
        }

        private System.Nullable<int> _S6FEES;

        public System.Nullable<int> S6FEES
        {
            get { return _S6FEES; }
            set { _S6FEES = value; }
        }

        private System.Nullable<int> _S7FEES;

        public System.Nullable<int> S7FEES
        {
            get { return _S7FEES; }
            set { _S7FEES = value; }
        }

        private System.Nullable<int> _S8FEES;

        public System.Nullable<int> S8FEES
        {
            get { return _S8FEES; }
            set { _S8FEES = value; }
        }

        private System.Nullable<int> _S9FEES;

        public System.Nullable<int> S9FEES
        {
            get { return _S9FEES; }
            set { _S9FEES = value; }
        }

        private System.Nullable<int> _S10FEES;

        public System.Nullable<int> S10FEES
        {
            get { return _S10FEES; }
            set { _S10FEES = value; }
        }

        private System.Nullable<System.DateTime> _FEESDATE;

        public System.Nullable<System.DateTime> FEESDATE
        {
            get { return _FEESDATE; }
            set { _FEESDATE = value; }
        }

        private string _IPADDRESS;

        public string IPADDRESS
        {
            get { return _IPADDRESS; }
            set { _IPADDRESS = value; }
        }

        private string _COLLEGE_CODE;

        public string COLLEGE_CODE
        {
            get { return _COLLEGE_CODE; }
            set { _COLLEGE_CODE = value; }
        }

    }
}
