﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.BusinessEntities.Administration
{
    /*                            
---------------------------------------------------------------------------------------------------------------------------                            
Created By : RUTUAJA DAWLE                     
Created On :  23-10-2023                   
Purpose :                         
Version :                         
---------------------------------------------------------------------------------------------------------------------------                            
Version  Modified On   Modified By      Purpose                            
---------------------------------------------------------------------------------------------------------------------------                             
--------------------------------------------------------------------------------------------------------------------------                                               
*/       
    public class ParameterList
    {
        int paramid { get; set; }
        string paramname { get; set; }
        int paramvalue { get; set; }
        string paramdescription { get; set; }
     
    }
}
